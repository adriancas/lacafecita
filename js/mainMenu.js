$(document).ready(() => {
    var menuOpened = false;
    $(".navMobil").append(`
    <span class="btn-switch-menu"><i class="fas fa-bars"></i></span>`);
    $("body").append("<div class='fade'></div>");
    $("body").append("<div class='modalBackground'></div>");
    $(".menuMobil span").html(`<i class="fas fa-bars"></i>`);
    $(".btn-switch-menu,.fade").click(() => {
        if (!menuOpened) {
            $(".menu").animate({ 'width': 'toggle' });
            menuOpened = !menuOpened;
            $(".fade").css("display", "flex");
            $("body").css("overflow-y", "hidden");
        } else {
            var fadeOpen = $(".fade").css("display");
            $(".menu").animate({ 'width': 'toggle' });
            $(".fade").css("display", "none");
            $("body").css("overflow-y", "scroll");
            menuOpened = !menuOpened;
            if (!fadeOpen) {
                $(".fade").css("display", "flex");
            }
        }
    });


    $(window).resize(function () {
        var width = $(window).width();
        var disp = $(".menu").css("display");
        if (width > 600) {
            if (disp == "none") {
                $(".menu").css("display", "flex");
                $(".fade").css("display", "none");
            }
        } else {
            $(".btn-switch-menu").click(() => {
                $(".fade").css("display", "flex");
            });
            $(".fade").click(() => {
                $(".fade").css("display", "none");
            });
            if (disp == "flex" || disp == "block") {
                $(".menu").css("display", "none");
                $(".fade").css("display", "none");
            }
        }
    });

})

