$(document).ready(() => {

    $("body").append("<div class='fade'></div>");
    $("#cardsOpen1").addClass("listMenuActive");
    $("#cards2, #cards3,#cards4,#cards5,#cards6").css("display", 'none');

    $(".linkOpenCards").click((e) => {
        let cardActive = e.target.id;
        let numCards = 5;
        for (let index = 1;index <= numCards;index++) {
            let c = "cardsOpen" + index;
            if (c == cardActive) {
                $("#cards" + index).fadeIn(2000);
                $("#" + c).addClass("listMenuActive");
            } else {
                $("#cards" + index).fadeOut(1000);
                $("#cardsOpen" + index).removeClass("listMenuActive");
            }
        }

    });

    $(".btn-ver").click(() => {
        $(".modal").css("display", "flex");
        $(".modalBackground").css("display", "flex");
    });


    $(".cerrarModal").click(() => {
        console.log("click background");
        $(".modal").css("display", "none");
        $(".modalBackground").css("display", "none");
    });

});
